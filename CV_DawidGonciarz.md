# _**CURRICULUM VITAE**_
---
### **DANE OSOBOWE**
**IMIĘ I NAZWISKO**: Dawid Gonciarz
**DATA URODZENIA**: dane wrażliwe
**ADRES**: dane wrażliwe
**TELEFON**: 513 172 729
**E-MAIL**: gonciarzdawid@gmail.com
---
### **WYKSZTAŁCENIE**
**2015-2018**: Uniwersytet Marii Curie-Skłodowskiej w Lublinie, Geoinformatyka, studia pierwszego stopnia  

---
### **DOŚWIADCZENIE**
 **08.2018 - obecnie**: Młodszy Specjalista GIS w Gospodarczym Instytucie Analiz Przestrzennych
* wektoryzacja pasa drogowego - dział produkcyjny

**07.2017 - 09.2017**: Terenowiec GIS w MAPPROJEKT  
* przeprowadzanie kontroli na miejscu metodą FOTO
---
### **OSIĄGNIĘCIA**
**05.2018**: Laureat II Akademickich Mistrzostw Geoinformatycznych _GIS Challenge 2018_

---
### **UMIEJĘTNOŚCI**
###### JĘZYKI:
* język angielski: poziom B2 (wyższy średnio zaawansowany)
*  język rosyjski: poziom A1 (początkujący)
###### INNE:
* Bardzo dobra znajomość oprogramowania QGIS oraz pakietu ArcGIS for Desktop
* Umiejętność obsługi sprzętu biurowego
* Bardzo dobra znajomość pakietu Microsoft Office
* Dobra znajomość sprzętu oraz oprogramowania GPS
* Doświadczenie w prowadzeniu badań hydrologicznych i hydrogeologicznych
* Doświadczenie w tworzeniu i zarządzaniu aplikacjami mobilnymi
* Prawo jazdy kategorii B
---
### **ZAINTERESOWANIA**
Zjawiska paranormalne, kryminologia, kinematografia amerykańska
